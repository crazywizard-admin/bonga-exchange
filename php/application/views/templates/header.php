<?php
$this->load->helper('url');
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title><?php echo $title; ?> -- Bonga Exchange</title>

    <!-- Bootstrap core CSS -->    
    <link href="<?php echo base_url('scripts/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template -->    
    <link href="<?php echo base_url('scripts/css/jumbotron-narrow.css'); ?>" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->    
    <script src="<?php echo base_url('scripts/js/ie-emulation-modes-warning.js'); ?>"></script>
    <script src="<?php echo base_url('scripts/css/bootstrap.min.css'); ?>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
	    .copy-left {
	     display: inline-block;
	     text-align: right;
	     margin: 0px;
	    -moz-transform: scaleX(-1);
	    -o-transform: scaleX(-1);
	    -webkit-transform: scaleX(-1);
	    transform: scaleX(-1);
	    filter: FlipH;
	    -ms-filter: "FlipH";
		}
    </style>
  </head>

    <body>

    <div class="container">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li role="presentation"><a href="<?php echo base_url('index.php/about'); ?>">About</a></li>
            <li role="presentation"><a href="#">Contact</a></li>
          </ul>
        </nav>
        <a href="<?php echo base_url(); ?>" style = "text-decoration: none;"><h3 class="text-muted">Bonga Exchange</h3></a>
      </div>