
      <div class="jumbotron">
        <h1>Start Trading ...</h1>        
        <p><span><a class="btn btn-lg btn-primary" href="<?php echo base_url('index.php/buy'); ?>" role="button">Buy Points</a></span>
        <span><a class="btn btn-lg btn-success" href="<?php echo base_url('index.php/sell'); ?>" role="button">Sell Points</a></span>
        </p>

      </div>

      <h4 class="sub-header">Latest Transactions </h4>
      <hr>
      <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>phone #</th>
                  <th>Points</th>                  
                  <th>Price @ unit</th>                  
                  <th>Time Ago</th>
                </tr>
              </thead>
              <tbody>
              	<tr>                
                <td>0721xxxx45</td>
                <td>30000</td>
                <td>0.56</td>                
                <td>2 mins</td>
                </tr>
                <tr>
                <td>0721xxxx45</td>
                <td>30000</td>
                <td>0.56</td>                
                <td>2 mins</td>
                </tr>
                <tr>
                <td>0721xxxx45</td>
                <td>30000</td>
                <td>0.56</td>                
                <td>2 mins</td>
                </tr>
                <tr>
                <td>0721xxxx45</td>
                <td>30000</td>
                <td>0.56</td>                
                <td>2 mins</td>
                </tr>
                <tr>
                <td>0721xxxx45</td>
                <td>30000</td>
                <td>0.56</td>                
                <td>2 mins</td>
                </tr>
                <tr>
                <td>0721xxxx45</td>
                <td>30000</td>
                <td>0.56</td>                
                <td>2 mins</td>
                </tr>
                <tr>
                <td>0721xxxx45</td>
                <td>30000</td>
                <td>0.56</td>                
                <td>2 mins</td>
                </tr>
                <tr>
                <td>0721xxxx45</td>
                <td>30000</td>
                <td>0.56</td>                
                <td>2 mins</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
