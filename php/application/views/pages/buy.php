    <script>
    function Initialize(id, seller, amount, price, min_offer, max_offer)
    {
    	//Initialize variables
    	var seller = seller;
    	var amount = amount;
    	var price = price;
    	var min_offer = min_offer;
    	var max_offer = max_offer;

    	$('#sellerid').text(seller);    	
    	$('#amount').prop('max', amount);
    	$('#unitprice').prop('placeholder', min_offer);
    	$('#unitprice').prop('min', min_offer);
    	$('#unitprice').prop('max', max_offer);
    	$('#post').prop('action', 'points/buy/'+id)
    	$('#max_amount').text('/ '+amount);
    }
    </script
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="sell">Selling</a></li>
            <li class="active"><a href="#">Buying</a></li>            
          </ul>          
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header"></h1>

          <h2 class="sub-header">Points on Offer <a href="sell" role="button" style="float:right;" class="btn btn-sm btn-success" >Sell Points</a></h2>
          <span class="validation-errors">
                      </span>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>phone #</th>
                  <th>Points</th>                  
                  <th>Unit @ Price</th>
                  <th>Expires</th>
                </tr>
              </thead>
              <tbody>
              <?php
              foreach($points->result() as $row)
              {
              	$price = round($row->amount/$row->price, 2);
              	$min_offer = round(0.5*$price);
              	$max_offer = round(1.5*$price);
              	$price_range = $min_offer ."-". $max_offer;
              	$posted = strtotime($row->posted);
              	$expires = time_elapsed_string($posted, $posted+604800);
              	$seller = substr_replace($row->seller, "xxxx", 3, 4);
              	$amount = $row->amount;

              	echo "<tr>
              	<td>$seller</td>
              	<td>$amount</td>
              	<td>$price_range</td>
              	<td>$expires</td>
              	<td><a href='#buyModal' role='button' class='btn btn-sm btn-primary announce' onclick='Initialize(\"$row->seller\", \"$seller\", $amount, $price, $min_offer, $max_offer)' data-toggle='modal' data-keyboard='false'>Buy</a></td>
              	</tr>";
              }
              ?>
              	
              </tbody>
            </table>
          </div>
        </div>
      </div>
    

    <!--Modal HTML -->
    <script>
		function Calculate()
		{
		var amount = $('#amount').val();
		var unitprice = $('#unitprice').val();
		if ((unitprice.length==0) || (amount.length==0))
		  {
		  	//Ignore
		  return;
		  }		
		else
		  {
		  	//Calculate
		  	var total = amount*unitprice;
		  	$('#totalprice').val(total);
		  }
		}
	</script>
    <div id="buyModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" >Make an Offer -- <span id="sellerid"></span></h4>
          </div>
          <div class="modal-body">
            <span class="validation-errors">
                      </span>
          <form action="" method="post" accept-charset="utf-8" class="form-horizontal" id="post">   
          <div class="row">                
                <div class="col-xs-6">
                    <div class="input-group">                    	
                        <input type="number" name="amount" class="form-control" placeholder="Amount" onkeyup="Calculate()" id="amount" min="0" max="" required>
                        <span class="input-group-addon" id="max_amount"></span>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="number" class="form-control" name="price" placeholder="" onkeyup="Calculate()" id="unitprice" min="" max=""required>
                        <span class="input-group-addon">.00</span>
                    </div>
                </div>
                <div class="col-xs-10"><p>
                    <div class="input-group">
                        <span class="input-group-addon">Ksh.</span>
                        <input type="number" class="form-control" name="totalprice" placeholder="Total" id="totalprice" required>
                        <span class="input-group-addon">.00</span>
                    </div></p>
                </div>
            </div>

                         
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          </form>
            </div>          
          </div>
        </div>
      </div>
    
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->    
    <script src="<?php echo base_url('scripts/js/jquery.min.js'); ?>"></script>    
    <script src="<?php echo base_url('scripts/js/bootstrap.min.js'); ?>"></script>       
  
  <?php
function time_elapsed_string($ptime, $ktime) {
    $etime = $ktime - $ptime;
    
    if ($etime < 1) {
        return '0 seconds';
    }
    
    $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
                );
    
    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . $str . ($r > 1 ? 's' : '');
        }
    }
  }
?>