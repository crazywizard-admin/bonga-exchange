    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Selling</a></li>
            <li><a href="buy">Buying</a></li>            
          </ul>          
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header"></h1>

          <h2 class="sub-header">Sell My Points </h2>
          <span class="validation-errors">
                      </span>
          <?php
          $attributes = array('role'=>'form');

          echo form_open('points/sell');
          ?>
          <form>
            <div class="row">                
                <div class="col-xs-4">
                    <div class="input-group">
                        <input type="number" name="amount" class="form-control" placeholder="Amount" required>
                        <span class="input-group-addon">.00</span>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="input-group">
                        <span class="input-group-addon">Ksh.</span>
                        <input type="number" class="form-control" name="price" placeholder="Price" required>
                        <span class="input-group-addon">.00</span>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="input-group">                        
                        <input type="submit" name="submit" style="float:right;" class="btn btn-sm btn-success" value="Sell Points">
                    </div>
                </div>                
            </div>
      </form>
      <h2 class="sub-header">My Transaction History <small><small>
      <?php
      $array = array("#E3E3E3"=>"Expired", "#4CB9DA"=>"On Offer", "#3D84C1"=>"On sale", "#53AD53"=>"Sold");
      foreach($array as $key=>$value)
      {
        echo " <span class='glyphicon glyphicon-stop' style='color:$key;'></span> $value ";
      }
      ?></small></small>
      </h2>

      <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>phone #</th>
                  <th>Points</th>                  
                  <th>Price @ unit</th> 
                  <th>Total Price</th>                 
                  <th>Posted</th>
                </tr>
              </thead>
              <tbody>
              <?php
              foreach($points->result() as $row)
              {
                if(!empty($row->buyer))
                  $phone = substr_replace($row->buyer, "xxxx", 3, 4);
                else
                  $phone = $row->seller;
                if(!empty($row->offer))
                  $price = $row->offer;
                else
                  $price = $row->price;                
                $posted = time_elapsed_string(strtotime($row->posted));
                $price_unit = round($row->amount/$row->price, 2);                                               
                switch($row->status)
                {
                  case -1:
                    {
                      $status = "Expired";
                      $color = "#E3E3E3";                      
                      break;
                    }
                  case 0:
                  {
                    $status = "On offer";
                    $color = "#4CB9DA";
                    break;
                  }
                  case 1:
                  {
                    $status = "On sale";
                    $color = "#3D84C1";
                    break;
                  }
                  case 2:
                  {
                    $status = "Sold";
                    $color = "#53AD53";
                    break;
                  }                  
                }
                echo "<tr>
                <td>$phone</td>
                <td>$row->amount</td>
                <td>$price_unit</td>                
                <td><span class='glyphicon glyphicon-stop' style='color:$color;'></span> $price /=</td>
                <td>$posted ago</td>
                </tr>";
              }
              ?>                
              </tbody>
            </table>
          </div>
      
        </div>
      </div>
    </div>

    <!--Modal HTML -->
    <div id="buyModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Make an Offer -- 07xxxxxxxx</h4>
          </div>
          <div class="modal-body">
            <span class="validation-errors">
                      </span>
          <form action="create" method="post" accept-charset="utf-8" class="form-horizontal">            
              <div class="form-group">
                <label class="control-label col-xs-4" for="regNum">Points : </label>
                <div class="col-xs-8">
                  <input type="text" class="form-control" name="reg" id="inputReg" placeholder="30,000" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-xs-4" for="owner">Price : </label>
                <div class="col-xs-8">
                  <input type="numeric" class="form-control" name="owner" id="inputOwner" placeholder="50" required="">
                </div>
              </div>            
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <input type="submit" class="btn btn-primary" value="Submit"></input>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>

    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->    
    <script src="<?php echo base_url('scripts/js/jquery.min.js'); ?>"></script>    
    <script src="<?php echo base_url('scripts/js/bootstrap.min.js'); ?>"></script>       
  
<?php
function time_elapsed_string($ptime) {
    $etime = time() - $ptime;
    
    if ($etime < 1) {
        return '0 seconds';
    }
    
    $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
                );
    
    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . $str . ($r > 1 ? 's' : '');
        }
    }
  }
?>
