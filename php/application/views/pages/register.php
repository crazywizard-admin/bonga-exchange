<?php
$this->load->helper('url');
$this->load->helper('form');
$this->load->library('session');
?>
<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>Login -- Bonga Exchange</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('scripts/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('scripts/css/signin.css'); ?>" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo base_url('scripts/js/ie-emulation-modes-warning.js'); ?>"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url('scripts/js/ie10-viewport-bug-workaround.js'); ?>"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .validation-errors{
      color: #D95932;
    }
    </style>
  </head>

  <body>

    <div class="container">

        
        <?php 
        $attributes = array('class'=>'form-signin', 'role'=>'form');

        echo form_open('accounts/register', $attributes);

        ?>
        <h2 class="form-signin-heading">Registration</h2>
        <span class="validation-errors">
          <?php           
          $msg = $this->session->flashdata('confirmation');
          echo (isset($msg) ? $msg : "");
          ?>
        </span>        
        <input type="phone" class="form-control" name="phone" placeholder="Phone Number" required="" autofocus="">        
        <div class="checkbox">
          
          <label style = "float:right;">
            <i>or</i> <a href = "login"><strong>Login</strong></a>
          </label>
        </div>
        <button class="btn btn-lg btn-success btn-block" type="submit">Register Me</button>
        
      </form>      

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  

</body></html>