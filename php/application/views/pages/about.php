<?php
$this->load->helper('url');
$this->load->helper('form');
$this->load->library('session');
?>

<!DOCTYPE html>

<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>About</title>

    <!-- Bootstrap core CSS -->    
    <link href="<?php echo base_url('scripts/css/bootstrap.min.css'); ?>" rel="stylesheet">    
    <link href="<?php echo base_url('scripts/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    
    <!-- Custom styles for this template -->    
    <link href="<?php echo base_url('scripts/css/starter-template.css'); ?>" rel="stylesheet">

      <!-- Custom styles for this template -->
    <link href="<?php echo base_url('scripts/css/signin.css'); ?>" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo base_url('scripts/js/ie-emulation-modes-warning.js'); ?>"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->    
    <script src="<?php echo base_url('scripts/js/ie10-viewport-bug-workaround.js'); ?>"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
h2{
    margin: 0;     
    color: #666;
    padding-top: 90px;
    font-size: 52px;
    font-family: "trebuchet ms", sans-serif;    
}
.container{
    margin: 0;
    padding-right: 50px;
    padding-left: 30px;
}

</style>
  </head>

  <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-home"> Home</span></a>
        </div>
        <div class="collapse navbar-collapse" style="float: right;">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active"><a href="#about">About</a></li>
            <li><a href="">Help</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">           
        <h3>About Bonga Exchange</h3>
        <p>
          Bonga Exchange is an open platform serving as an exchange for buying and selling Bonga Points. Anybody can list
          points for selling as well as buy listed points.
        </p>
        
        <h3>Copyleft and License</h3>
        <p>
            Bonga Exchange is community built adn therefore open to anyone. The code is open and anyone wishing to contribute to the project can do so but cloning the GIT and working on the code.
        </p>

        <h3>Accessing the Exchange</h3>
        <p>
            Pages on the exchange are globally viewable and indexable. However, to trade, you'll need to authenticate a real phone number for use in trading.
        </p>

        <h3>Contributions</h3>
        <p>
            You can contribute to the project by using the exchange and emailing us any bugs or feature requests you have. Also you can join the development team in any position, including(but not limited to) coding, QA Testing, design and publicity.

            If you feel you don't fit into any of these roles but still want to contribute, you can help us pay for servers, bandwidth, coffee & Redbull and we'll make a special mention on your name in out credits.  :)
        </p>

        <h3>Still Having Trouble?</h3>
        <p>
            If you run into any bugs, please feel free to contact us. If you have solved a bug, you can submit a pull request with the bug fix on the GIT. For additional support and other questions, please <a href="mailto:crazywizard@kichizi.com"><strong>email</strong></a> us and we will try our best to assist you.
        </p>

      <footer>
      <div class="container" align="center">
        <p>Created by <a href="#" rel="crazywizard">@me</a>. &copy 2014</p>
      </div>
    </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->    
    <script src="<?php echo base_url('scripts/js/jquery.min.js'); ?>"></script>    
    <script src="<?php echo base_url('scripts/js/bootstrap.min.js'); ?>"></script>

</body></html>