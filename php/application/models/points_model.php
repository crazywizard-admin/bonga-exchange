<?php
class Points_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
		$this->load->library('session');
	}

	public function create_points()
	{
		$data = array(
			'seller'=>$this->session->userdata('username'),
			'amount'=>$this->input->post('amount'),
			'price'=>$this->input->post('price'),
			'status'=>0			
			);
		$this->db->set('posted', 'NOW()', FALSE);

		return $this->db->insert('points', $data);
	}

	public function get_points()
	{
		$this->db->order_by('posted', 'desc');
		$this->db->where('status !=', 2 );
		$this->db->where('posted >= CURDATE()-interval 7 day');		
		$query = $this->db->get('points');
		return $query;
	}

	public function get_my_points($username)
	{
		$this->db->order_by('posted', 'desc');		
		$this->db->where('seller', $username);

		return $this->db->get('points');
	}
}
?>