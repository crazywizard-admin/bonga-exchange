<?php
class Accounts_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function create_account()
	{		
		//Generate PIN
		//$pin = rand(1000,9999);
		$pin = 1234;
		$code = md5($pin);		
		$data = array(
			'phone'=>$this->input->post('phone'),
			'status'=>0,
			'code'=>$code
			);
		$this->db->set('timestamp', 'NOW()', FALSE);

		return $this->db->insert('users', $data);
	}

	public function exists($phone)
	{//Check if account registered under email exists		
		$this->db->where('phone', $phone);
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function login($verbose=FALSE)
	{
		$phone = $this->input->post('phone');
		$code = $this->input->post('code');
		
		$this->db->where('phone', $phone);
		$this->db->where('code', md5($code));
		$query = $this->db->get('users');
		//$this->db->where('confirmed', 1);		
		if(($verbose==TRUE)&&($query->num_rows()>0))
		{
			return $query->row();
		}
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

}
?>