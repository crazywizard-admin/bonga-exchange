<?php
class Pages extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('custom');
		$this->load->helper('url');
		$this->load->library('user_agent');
		$this->load->model('points_model');
	}

	public function view($page='home', $id=null)
	{
		if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}

		if($page=='home')
		{
			$data['title'] = ucfirst($page);	//Capitalize the first letter

			$this->load->view('templates/header', $data);
			$this->load->view('pages/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}else if($page=='login')
		{
			$data['title'] = ucfirst($page);

			$this->load->view('pages/'.$page, $data);
		}else if($page=='register')
		{
			$data['title'] = ucfirst($page);

			$this->load->view('pages/'.$page, $data);
		}else if($page=='buy')
		{
			logged_in_check($this->session->userdata('logged_in'));

			//View available points
			$data['points'] = $this->points_model->get_points();

			$data['title'] = ucfirst($page);

			$this->load->view('templates/header2');
			$this->load->view('pages/'.$page, $data);
			$this->load->view('templates/footer');
		}else if($page=='sell')
		{
			logged_in_check($this->session->userdata('logged_in'));

			//View my history
			$data['points'] = $this->points_model->get_my_points($this->session->userdata('username'));

			$data['title'] = ucfirst($page);

			$this->load->view('templates/header2');
			$this->load->view('pages/'.$page, $data);
			$this->load->view('templates/footer');
		}else if($page=='about')
		{
			$data['title'] = ucfirst($page);
			
			$this->load->view('pages/'.$page, $data);			
		}
		
	}
}
?>