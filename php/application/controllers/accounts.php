<?php
class Accounts extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('accounts_model');		
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('custom');
	}

	public function register()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('phone', 'Number', 'trim|required|xss_clean');

		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('pages/register');
		}
		else
		{
			//Success
			$this->accounts_model->create_account();			
			$this->session->set_flashdata('Confirmation', 'Please Enter the Confirmation PIN');
			$this->load->view('pages/login');
		}
	}

	public function login()
	{
		$this->load->helper('form');

		$login_result = $this->accounts_model->login(TRUE);
		
		if($login_result)
		{
			//Success
			$user_data = array(
				'username'=>$login_result->phone,
				'logged_in'=>TRUE
				);
			$this->session->set_userdata($user_data);
			//Redirect to next screen
			/* TO-DO: redirect to next intended screen */
			logged_in_check(TRUE);
			redirect('home');
		}else
		{
			$this->session->set_flashdata('confirmation', 'Invalid phone/code combination!');
			//$this->load->view('pages/login');
			redirect('login');
		}
	}

	public function login_redirect()
	{
		redirect('login');
	}

	public function logout()
	{
		$user_data = array('username'=>null, 'logged_in'=>FALSE);
		$this->session->unset_userdata($user_data);

		redirect('home');
	}
}
?>