<?php
class Points extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('points_model');
		$this->load->helper('url');
	}

	public function sell()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		//Validate form
		$this->form_validation->set_rules('amount', 'Number', 'trim|required|xss_clean');
		$this->form_validation->set_rules('price', 'Number', 'trim|required|xss_clean');

		if($this->form_validation->run() === FALSE)
		{
			//Failed
			redirect('sell');
		}else
		{
			//Success
			$this->points_model->create_points();
			redirect('sell');
		}
	}

	public function buy($id)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		//Validate form
		$this->form_validation->set_rules('phone1', 'Number', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Number', 'trim|required|xss_clean');
		$this->form_validation->set_rules('price', 'Number', 'trim|required|xss_clean');
		$this->form_validation->set_rules('totalprice', 'Number', 'trim|required|xss_clean');

		if($this->form_validation->run() === FALSE)
		{
			//Set Error message
			redirect('buy');
		}else
		{
			//Success
			$this->points_model->save_offer();
			//Set message
			redirect('buy');
		}
	}
}
?>