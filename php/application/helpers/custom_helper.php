<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('logged_in_check'))
{	
	function logged_in_check($logged_in)
	{		
		$ci_this =& get_instance();
		if($logged_in != TRUE)
		{
			//Set referrer
			$referrer = current_url();
			$navigation = array('referrer'=>$referrer);			
			$ci_this->session->set_userdata($navigation);
			//Redirect to login page
			redirect('login');
			
			exit();
		}else
		{
			//Check if referrer is set and reset
			if (!($ci_this->session->userdata('referrer')))
			{
				$referrer = $ci_this->session->userdata('referrer');						
				$ci_this->session->unset_userdata('referrer');
				redirect($referrer);

				exit();
			}

		}
	}
}
?>